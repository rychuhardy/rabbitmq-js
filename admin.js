#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var ex = 'exchange1';

    ch.assertExchange(ex, 'topic', {durable: false});
    // ch.prefetch(1);

    ch.assertQueue('', {exclusive: true}, function(err, q) {
      ch.bindQueue(q.queue, ex, '#');

      ch.consume(q.queue, function(msg) {
        console.log(" [x] Received message: %s:'%s'", msg.fields.routingKey, msg.content.toString());
      }, {noAck: true});

      var stdin = process.openStdin();
      
      stdin.addListener("data", function(d) {
        var input = d.toString().trim();

        ch.publish(ex, 'admin', new Buffer(input));

      });
    });
  });
});