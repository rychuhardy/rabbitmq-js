#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

var args = process.argv.slice(2);

if (args.length == 0) {
  console.log("Usage: technician.js [knee] [elbow] [ankle]");
  process.exit(1);
}

amqp.connect('amqp://localhost', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var ex = 'exchange1';
    ch.assertExchange(ex, 'topic', {durable: false});
    ch.prefetch(1);

    ch.assertQueue('', {exclusive: true}, function(err, q){
      ch.bindQueue(q.queue, ex, 'admin');

      ch.consume(q.queue, function(msg) {
        console.log("[x] Received admin info: %s", msg.content.toString());
      }, {noAck: true});
    });
    
    args.forEach(function(bodypart) {
      ch.assertQueue(bodypart, {durable: false});
      ch.bindQueue(bodypart, ex, bodypart);

      ch.consume(bodypart, function reply(msg) {
        var content = msg.content.toString();

        console.log("Otrzymano zlecenie: " + content + " typu: " + bodypart);
        setTimeout(function() {
          console.log(" [x] Badanie wykonane");
          ch.publish(ex, msg.properties.replyTo, new Buffer(content));
          ch.ack(msg);
        }, 3000);
      });
    });
    console.log("[x] Oczekiwanie na zlecenie");
  });
});
