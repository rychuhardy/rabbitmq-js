#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

console.log("Usage: [injury type] [patient name]");

amqp.connect('amqp://localhost', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var ex = "exchange1";
    ch.assertExchange(ex, 'topic', {durable: false});

    ch.assertQueue('', {exclusive: true}, function(err, q) {
      ch.bindQueue(q.queue, ex, q.queue);
      ch.bindQueue(q.queue, ex, 'admin');
      
      var stdin = process.openStdin();
      
      stdin.addListener("data", function(d) {
        // note:  d is an object, and when converted to a string it will
        // end with a linefeed.
        var input = d.toString().trim();
        var words = input.split(" ");
        var type = words[0];
        var name = words.slice(1).join(" ");

        ch.consume(q.queue, function(msg) {
          if(msg.fields.routingKey == "admin") {
            console.log("[x] Received admin info: %s", msg.content.toString());
          } else {
            console.log(' [x] Otrzymano wynik: %s', msg.content.toString());
        }
        }, {noAck: true});

        ch.publish(ex, type, new Buffer(name), { replyTo: q.queue });
      });
    });
  });
});